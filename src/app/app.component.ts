import {Component, Inject, LOCALE_ID} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  languages = [
    { code: 'en', label: 'English'},
    { code: 'fr', label: 'Français'},
    { code: 'ro', label: 'Romana'}
  ];

  constructor(@Inject(LOCALE_ID) protected localeId: string) {}

  minutes = 0;
  gender = 'f';
  logo = 'https://angular.io/assets/images/logos/angular/angular.png';
  inc(i: number) {
    this.minutes = Math.min(5, Math.max(0, this.minutes + i));
  }
  male() { this.gender = 'm'; }
  female() { this.gender = 'f'; }
  other() { this.gender = 'o'; }
}
